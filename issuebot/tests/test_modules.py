#!/usr/bin/env python3

import issuebot
import os
import unittest
from tempfile import TemporaryDirectory
from unittest import mock


class TestModules(unittest.TestCase):
    """Test the setup of the harness for running the modules"""

    def setUp(self):
        self.cwd = os.getcwd()
        self.environ = os.environ
        os.environ['CI'] = '1'

    def tearDown(self):
        for k in self.environ.keys():
            os.environ[k] = self.environ[k]
        os.chdir(self.cwd)

    def test_get_modules(self):
        """get_modules should return the local modules when run like ./issuebot.py"""
        os.chdir(os.path.dirname(os.path.dirname(__file__)))
        self.assertTrue(issuebot.get_modules())

    def test_print_exc_flush(self):
        """print_exc_flush should print in the same timeline as print"""
        print('BEFORE', flush=True)
        try:
            int('this should throw an exception between "BEFORE" and "AFTER"')
        except:
            issuebot.print_exc_flush()
        print('AFTER', flush=True)

    def test_setup_for_modules_with_py(self):
        def run_cli_tool(cmd):
            if cmd[0] == 'apt-get':
                self.count += 1
                self.assertEqual('androguard', cmd[2])
                self.assertEqual('s3cmd', cmd[3])

        self.count = 0
        testfile = 'modules/foo.py'
        with TemporaryDirectory() as tmpdir, mock.patch(
            'issuebot.run_cli_tool', run_cli_tool
        ), mock.patch('issuebot.get_modules', lambda: [testfile]), mock.patch(
            'os.getuid', lambda: 0
        ):
            os.chdir(tmpdir)
            os.mkdir('modules')
            with open(testfile, 'w') as fp:
                fp.write('# issuebot_apt_install = androguard s3cmd\n')
            issuebot.setup_for_modules()
            with open(testfile, 'w') as fp:
                fp.write('#issuebot_apt_install = androguard s3cmd\n')
            issuebot.setup_for_modules()
            with open(testfile, 'w') as fp:
                fp.write('  #   issuebot_apt_install = androguard   s3cmd\n')
            issuebot.setup_for_modules()
            with open(testfile, 'w') as fp:
                fp.write('#\tissuebot_apt_install = androguard\ts3cmd\n')
            issuebot.setup_for_modules()
        self.assertEqual(4, self.count)

    def test_setup_for_modules_with_php(self):
        def run_cli_tool(cmd):
            if cmd[0] == 'apt-get':
                self.count += 1
                self.assertEqual('php-cli', cmd[2])
                self.assertEqual('apktool', cmd[3])

        self.count = 0
        testfile = 'modules/foo.php'
        with TemporaryDirectory() as tmpdir, mock.patch(
            'issuebot.run_cli_tool', run_cli_tool
        ), mock.patch('issuebot.get_modules', lambda: [testfile]), mock.patch(
            'os.getuid', lambda: 0
        ):
            os.chdir(tmpdir)
            os.mkdir('modules')
            with open(testfile, 'w') as fp:
                fp.write('// issuebot_apt_install = php-cli apktool\n')
            issuebot.setup_for_modules()
            with open(testfile, 'w') as fp:
                fp.write('/*\nissuebot_apt_install = php-cli apktool\n*/\n')
            issuebot.setup_for_modules()
            with open(testfile, 'w') as fp:
                fp.write('/*issuebot_apt_install = php-cli apktool*/\n')
            issuebot.setup_for_modules()
        self.assertEqual(3, self.count)

    def test_get_current_commit_id_url(self):
        test_tags = [
            (
                'https://github.com/ukanth/afwall',
                'v3.5.2',
                'https://github.com/ukanth/afwall/tree/v3.5.2',
            ),
            (
                'https://gitlab.com/fdroid/fdroidclient',
                '1.12.1',
                'https://gitlab.com/fdroid/fdroidclient/tree/1.12.1',
            ),
            (
                'https://codeberg.org/Arne/monocles_chat',
                'v1.1',
                'https://codeberg.org/Arne/monocles_chat/src/v1.1',
            ),
            (
                'https://notabug.org/Tobiwan/ffupdater',
                '74.1.0',
                'https://notabug.org/Tobiwan/ffupdater/src/74.1.0',
            ),
            (
                'https://framagit.org/tom79/fedilab-tube',
                '1.13.1',
                'https://framagit.org/tom79/fedilab-tube/tree/1.13.1',
            ),
            (
                'https://git.sr.ht/~anjan/lift',
                'v0.2',
                'https://git.sr.ht/~anjan/lift/tree/v0.2',
            ),
            (
                'https://git.jami.net/savoirfairelinux/ring-project',
                'android/release_296',
                'https://git.jami.net/savoirfairelinux/ring-project/tree/android/release_296',
            ),
            (
                'https://git.selfprivacy.org/kherel/selfprivacy.org.app',
                'v.0.1.2',
                'https://git.selfprivacy.org/kherel/selfprivacy.org.app/src/v.0.1.2',
            ),
            (
                'https://git.selfprivacy.org/kherel/selfprivacy.org.app',
                'd3f494adebc897ba003e059baf028a33c1e46336',
                'https://git.selfprivacy.org/kherel/selfprivacy.org.app/src/d3f494adebc897ba003e059baf028a33c1e46336',
            ),
        ]
        issuebot_module = issuebot.IssuebotModule()
        with TemporaryDirectory() as tmpdir:
            os.chdir(tmpdir)
            issuebot_module.source_dir = os.path.join(os.getcwd(), 'test')
            os.mkdir(issuebot_module.source_dir)
            os.mkdir(os.path.join(issuebot_module.source_dir, '.git'))
            for source_url, commit_id, tag_url in test_tags:
                issuebot_module.source_url = source_url
                self.assertEqual(
                    issuebot_module.get_current_commit_id_url(commit_id), tag_url
                )

    def test_get_job_issue_ids(self):
        if 'CI_JOB_ID' in os.environ:
            self.assertEqual(os.getenv('CI_JOB_ID'), issuebot.get_job_issue_ids()[0])
            del os.environ['CI_JOB_ID']
        if 'ISSUEBOT_CURRENT_ISSUE_ID' in os.environ:
            self.assertEqual(
                os.getenv('ISSUEBOT_CURRENT_ISSUE_ID'), issuebot.get_job_issue_ids()[1]
            )
            del os.environ['ISSUEBOT_CURRENT_ISSUE_ID']
        self.assertTrue('0', issuebot.get_job_issue_ids()[1])

    def test_run_cli_tool_smokecheck(self):
        """Test that cli tools work at the most basic level"""
        p = issuebot.run_cli_tool(['date'])
        print(p.stdout.decode())

    def test_run_cli_tool_timeout(self):
        """Test that things exit cleanly when the timeout is hit"""
        issuebot.run_cli_tool(['sleep', '2'], timeout=1)

    def test_run_cli_tool_error(self):
        """Test that things exit cleanly when the tool returns an error"""
        issuebot.run_cli_tool(['false'], timeout=1)


if __name__ == "__main__":
    unittest.main()
