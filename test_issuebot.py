#!/usr/bin/env python3

import configparser
import inspect
import issuebot
import os
import shutil
import tempfile
import unittest
import yaml
import zipfile
from pathlib import Path
from unittest.mock import MagicMock, patch


BASEDIR = Path(__file__).resolve().parent


def _mock_download_file(url):
    """Pretend to download the URL and create a local APK"""
    repo = Path('repo')
    repo.mkdir(exist_ok=True)
    apkfile = repo / os.path.basename(url)
    shutil.copy(
        str(BASEDIR / ('issuebot/tests/org.bitbucket.tickytacky.mirrormirror_4.apk')),
        str(apkfile),
    )
    return str(apkfile.resolve())


class IssuebotTest(unittest.TestCase):
    """test the core issuebot runner script"""

    def setUp(self):
        self.tempdir = tempfile.TemporaryDirectory()
        os.chdir(self.tempdir.name)
        datadir = Path('data')
        datadir.mkdir()
        for f in (BASEDIR / datadir).glob('*.*'):
            # TODO in bullseye, str() is no longer needed
            shutil.copy(str(f), str(datadir))

    def tearDown(self):
        self.tempdir.cleanup()
        if 'ISSUEBOT_PROCESS_WITHOUT_SOURCE' in os.environ:
            del os.environ['ISSUEBOT_PROCESS_WITHOUT_SOURCE']
        if 'ISSUEBOT_CURRENT_APPLICATION_ID' in os.environ:
            del os.environ['ISSUEBOT_CURRENT_APPLICATION_ID']

    def _issue_with_helpful_logs(self, frame):
        issue = MagicMock()
        issue.get_id = lambda: inspect.getframeinfo(frame).lineno
        issue.title = inspect.getframeinfo(frame).function
        return issue

    def test_valid_appid_regex(self):
        should_be = [
            'foo.bar',
            'com.moez.QKSMS',
            'An.stop',
            'SpeedoMeterApp.main',
            'a2dp.Vol',
            'click.dummer.UartSmartwatch',
            'com.Bisha.TI89EmuDonation',
            'org.f_droid.fdr0ID',
        ]
        found = []
        for appid in issuebot.APPLICATION_ID_PATTERN.findall(
            """
            ApplicationID:
               foo.bar
            Application ID: com.moez.QKSMS
            package name = "An.stop",
            package ID:		SpeedoMeterApp.main
            packageName:a2dp.Vol
            packageName: 'click.dummer.UartSmartwatch'
            Package = 'com.Bisha.TI89EmuDonation'
            appid = 'org.f_droid.fdr0ID'
        """
        ):
            found.append(appid)
        self.assertEqual(should_be, found)

    def test_invalid_appid_regex(self):
        valid = []
        for appid in issuebot.APPLICATION_ID_PATTERN.findall(
            """
            ID: foo.bar
            ApplicationID: 0.com.moez.QKSMS
            package name = "_An.stop",
            pID: "SpeedoMeterApp.main",
            pkgName:a2dp.Vol
            pkg 'click.dummer.UartSmartwatch'
            Name = 'com.Bisha.TI89EmuDonation'
            apid = 'org.f_droid.fdr0ID'
        """
        ):
            print(appid)
            valid.append(appid)
        self.assertEqual(0, len(valid))

    def test_create_dummy_fdroid_repo(self):
        assert not os.path.exists('config.yml')
        issuebot.create_dummy_fdroid_repo()
        assert os.path.isdir('build')
        assert os.path.isdir('metadata')
        assert os.path.isdir('repo')
        assert not os.path.exists('config.py')
        with open('config.yml') as fp:
            yaml.safe_load(fp)

    @patch('issuebot.get_apk_from_github')
    @patch('issuebot.process_issue_or_merge_request')
    @patch('issuebot.run_cli_tool')
    def test_process_issue_nothing_to_parse(
        self, run_cli_tool, process_issue_or_merge_request, get_apk_from_github
    ):
        issue = self._issue_with_helpful_logs(inspect.currentframe())
        issue.description = "There is nothing to parse here"
        issuebot.process_issue(MagicMock(), issue)
        assert not get_apk_from_github.called
        assert not run_cli_tool.called
        self.assertEqual(issue, process_issue_or_merge_request.call_args[0][1])
        self.assertIsNone(process_issue_or_merge_request.call_args[0][2])
        self.assertEqual([], process_issue_or_merge_request.call_args[0][3])
        assert 'git-url' not in process_issue_or_merge_request.call_args[0][4]

    @patch('issuebot.get_apk_from_github')
    @patch('issuebot.process_issue_or_merge_request')
    @patch('issuebot.run_cli_tool')
    def test_process_issue_git_url_only(
        self, run_cli_tool, process_issue_or_merge_request, get_apk_from_github
    ):
        url = "https://gitlab.com/com/foo"
        issue = self._issue_with_helpful_logs(inspect.currentframe())
        issue.description = url
        issuebot.process_issue(MagicMock(), issue)
        assert get_apk_from_github.called
        self.assertEqual(['fdroid', 'import'], run_cli_tool.call_args[0][0][0:2])
        self.assertEqual(issue, process_issue_or_merge_request.call_args[0][1])
        self.assertIsNone(process_issue_or_merge_request.call_args[0][2])
        self.assertEqual([url], process_issue_or_merge_request.call_args[0][3])
        assert 'git-url' in process_issue_or_merge_request.call_args[0][4]

    @patch('issuebot.get_modules')
    @patch('issuebot.get_apk_from_github')
    @patch('issuebot.get_apk_from_huawei_app_gallery')
    @patch('issuebot.get_apk_from_izzysoft')
    @patch('issuebot.run_cli_tool')
    def test_process_issue_git_url_and_appid(
        self,
        run_cli_tool,
        get_apk_from_izzysoft,
        get_apk_from_huawei_app_gallery,
        get_apk_from_github,
        get_modules,
    ):
        appid = 'com.foo'
        url = "https://gitlab.com/com/foo"
        issue = self._issue_with_helpful_logs(inspect.currentframe())
        issue.description = (
            url + "\nhttps://play.google.com/store/apps/details?id=" + appid
        )

        def _mock_import(cmd, cwd=None, timeout=600):  # pylint: disable=W0613
            if cmd[1] == 'import':
                os.mkdir('metadata')
                with open(issuebot.get_metadata_file(appid), 'w') as fp:
                    fp.write('SourceCode: ' + cmd[3])
                    fp.write('\n')
            return MagicMock()

        run_cli_tool.side_effect = _mock_import
        issuebot.process_issue(MagicMock(), issue)
        assert get_modules.called
        assert get_apk_from_github.called
        assert get_apk_from_huawei_app_gallery.called
        assert get_apk_from_izzysoft.called
        self.assertEqual(
            ['fdroid', 'import'], run_cli_tool.call_args_list[0][0][0][0:2]
        )
        self.assertEqual(
            ['fdroid', 'scanner'], run_cli_tool.call_args_list[1][0][0][0:2]
        )
        self.assertEqual(
            ['fdroid', 'update'], run_cli_tool.call_args_list[2][0][0][0:2]
        )
        assert 'git-url' in issue.labels
        assert appid in issue.labels

    @patch('issuebot.download_file', side_effect=_mock_download_file)
    @patch('issuebot.get_apk_from_github')
    @patch('issuebot.get_apk_from_huawei_app_gallery')
    @patch('issuebot.run_cli_tool')
    def test_process_issue_apk_download_url(
        self,
        run_cli_tool,
        get_apk_from_github,
        get_apk_from_huawei_app_gallery,
        download_file,
    ):
        url = "https://example.com/com.foo.apk"
        issue = self._issue_with_helpful_logs(inspect.currentframe())
        issue.description = url
        issuebot.process_issue(MagicMock(), issue)
        self.assertEqual(url, download_file.call_args[0][0])
        self.assertEqual(['fdroid', 'update'], run_cli_tool.call_args[0][0][0:2])
        assert os.path.exists('repo/com.foo.apk')
        assert not get_apk_from_github.called
        assert not get_apk_from_huawei_app_gallery.called

    @patch('issuebot.get_apk_from_github')
    @patch('issuebot.process_issue_or_merge_request')
    @patch('issuebot.run_cli_tool')
    def test_process_issue_too_many_git_urls(
        self, run_cli_tool, process_issue_or_merge_request, get_apk_from_github
    ):
        url = "https://gitlab.com/com/foo"
        issue = self._issue_with_helpful_logs(inspect.currentframe())
        issue.description = '%s0\n%s1\n%s2\n' % (url, url, url)
        issuebot.process_issue(MagicMock(), issue)
        assert get_apk_from_github.called
        self.assertEqual(url + '2', run_cli_tool.call_args[0][0][3])
        self.assertEqual(issue, process_issue_or_merge_request.call_args[0][1])
        self.assertIsNone(process_issue_or_merge_request.call_args[0][2])
        self.assertEqual(3, len(process_issue_or_merge_request.call_args[0][3]))
        assert 'git-url' in process_issue_or_merge_request.call_args[0][4]
        assert 'too-many-git-urls' in process_issue_or_merge_request.call_args[0][4]

    @patch('issuebot.get_apk_from_github')
    @patch('issuebot.process_issue_or_merge_request')
    @patch('issuebot.run_cli_tool')
    def test_process_issue_too_many_appids(
        self, run_cli_tool, process_issue_or_merge_request, get_apk_from_github
    ):
        url = "https://play.google.com/store/apps/details?id="
        appid = 'com.foo'
        issue = self._issue_with_helpful_logs(inspect.currentframe())
        d = 'app \n{url}{appid}0\n{url}{appid}1\n{url}{appid}2\n'
        issue.description = d.format(appid=appid, url=url)
        issuebot.process_issue(MagicMock(), issue)
        assert not get_apk_from_github.called
        assert not run_cli_tool.called
        self.assertEqual(issue, process_issue_or_merge_request.call_args[0][1])
        self.assertIsNone(process_issue_or_merge_request.call_args[0][2])
        self.assertEqual([], process_issue_or_merge_request.call_args[0][3])
        self.assertTrue(
            'too-many-ApplicationIds' in process_issue_or_merge_request.call_args[0][4]
        )

    @patch('issuebot.get_apk_from_github')
    @patch('issuebot.process_issue_or_merge_request')
    @patch('issuebot.run_cli_tool')
    def test_process_issue_appid_only(
        self, run_cli_tool, process_issue_or_merge_request, get_apk_from_github
    ):
        appid = 'com.foo'
        issue = self._issue_with_helpful_logs(inspect.currentframe())
        issue.description = "https://play.google.com/store/apps/details?id=" + appid
        issuebot.process_issue(MagicMock(), issue)
        assert not get_apk_from_github.called
        assert not run_cli_tool.called
        self.assertEqual(issue, process_issue_or_merge_request.call_args[0][1])
        self.assertIsNone(process_issue_or_merge_request.call_args[0][2])
        self.assertEqual([], process_issue_or_merge_request.call_args[0][3])
        assert appid not in process_issue_or_merge_request.call_args[0][4]
        assert 'git-url' not in process_issue_or_merge_request.call_args[0][4]

    @patch('issuebot.get_apk_from_github')
    @patch('issuebot.get_apk_from_izzysoft')
    @patch('issuebot.get_modules')
    @patch('issuebot.run_cli_tool')
    def test_process_issue_appid_only_allowed(
        self, run_cli_tool, get_modules, get_apk_from_izzysoft, get_apk_from_github
    ):
        appid = 'com.foo'
        issue = self._issue_with_helpful_logs(inspect.currentframe())
        issue.title = "please package Foo"
        issue.description = "https://play.google.com/store/apps/details?id=" + appid
        os.environ['ISSUEBOT_PROCESS_WITHOUT_SOURCE'] = 'true'
        issuebot.create_dummy_fdroid_repo()
        issuebot.process_issue(MagicMock(), issue)
        assert not get_apk_from_github.called
        assert get_apk_from_izzysoft.called
        assert get_modules.called
        self.assertEqual(
            ['fdroid', 'scanner'], run_cli_tool.call_args_list[0][0][0][0:2]
        )
        self.assertEqual(
            ['fdroid', 'update'], run_cli_tool.call_args_list[1][0][0][0:2]
        )
        assert appid in issue.labels
        assert 'git-url' not in issue.labels

    @patch('issuebot.download_file', side_effect=_mock_download_file)
    @patch('issuebot.get_apk_from_github')
    @patch('issuebot.get_apk_from_huawei_app_gallery')
    @patch('issuebot.get_apk_from_izzysoft')
    @patch('issuebot.get_modules')
    @patch('issuebot.run_cli_tool')
    def test_process_issue_two_appids(
        self,
        run_cli_tool,
        get_modules,
        get_apk_from_izzysoft,
        get_apk_from_huawei_app_gallery,
        get_apk_from_github,
        download_file,
    ):
        """Test when the issue being processed is not the first in the job run"""
        issuebot.create_dummy_fdroid_repo()
        appid = 'org.bitbucket.tickytacky.mirrormirror'
        issue = self._issue_with_helpful_logs(inspect.currentframe())
        issue.title = "please package Foo"
        issue.description = "https://foo.com/%s.apk, appid: %s" % (appid, appid)
        os.environ['ISSUEBOT_PROCESS_WITHOUT_SOURCE'] = 'true'

        # setup an app that was already processed, e.g. a separate issue
        prev_appid = 'info.zwanenburg.caffeinetile'
        shutil.copy(str(BASEDIR / ('issuebot/tests/%s_4.apk' % prev_appid)), 'repo')
        Path(issuebot.get_metadata_file(prev_appid)).open('w').close()

        issuebot.process_issue(MagicMock(), issue)
        assert not get_apk_from_github.called
        assert get_apk_from_huawei_app_gallery.called
        assert get_apk_from_izzysoft.called
        assert get_modules.called
        assert download_file.called
        self.assertEqual(
            ['fdroid', 'scanner'], run_cli_tool.call_args_list[0][0][0][0:2]
        )
        self.assertEqual(
            ['fdroid', 'update'], run_cli_tool.call_args_list[1][0][0][0:2]
        )
        assert appid in issue.labels
        assert prev_appid not in issue.labels
        assert 'git-url' not in issue.labels

    def test_IssuebotModule_apkfiles(self):
        issuebot.create_dummy_fdroid_repo()
        mirrormirror = 'repo/mirrormirror.apk'
        shutil.copy(
            str(BASEDIR / 'issuebot/tests/org.bitbucket.tickytacky.mirrormirror_4.apk'),
            mirrormirror,
        )
        org_fdroid_ci = 'repo/all.dots.and.more.apk'
        shutil.copy(
            str(BASEDIR / 'issuebot/tests/minimal_targetsdk_30_unsigned.apk'),
            org_fdroid_ci,
        )
        # filename mocks the appid of a different app
        caffeinetile = 'repo/org.bitbucket.tickytacky.mirrormirror_999.apk'
        shutil.copy(
            str(BASEDIR / 'issuebot/tests/info.zwanenburg.caffeinetile_4.apk'),
            caffeinetile,
        )

        os.environ['ISSUEBOT_CURRENT_APPLICATION_ID'] = 'fake.app.id'
        self.assertEqual([], issuebot.IssuebotModule().apkfiles)

        os.environ['ISSUEBOT_CURRENT_APPLICATION_ID'] = 'org.fdroid.ciNO'
        self.assertEqual([], issuebot.IssuebotModule().apkfiles)

        os.environ['ISSUEBOT_CURRENT_APPLICATION_ID'] = 'org.fdroid.ci'
        self.assertTrue(issuebot.IssuebotModule().apkfiles[0].endswith(org_fdroid_ci))

        os.environ['ISSUEBOT_CURRENT_APPLICATION_ID'] = 'info.zwanenburg.caffeinetile'
        self.assertTrue(issuebot.IssuebotModule().apkfiles[0].endswith(caffeinetile))

        os.environ[
            'ISSUEBOT_CURRENT_APPLICATION_ID'
        ] = 'org.bitbucket.tickytacky.mirrormirror'
        self.assertTrue(issuebot.IssuebotModule().apkfiles[0].endswith(mirrormirror))

    def test_IssuebotModule_bad_apkfiles(self):
        issuebot.create_dummy_fdroid_repo()
        good = 'repo/caffeinetile.apk'
        shutil.copy(
            str(BASEDIR / 'issuebot/tests/info.zwanenburg.caffeinetile_4.apk'), good
        )
        bad = 'repo/zip-but-not-an.apk'
        with zipfile.ZipFile(bad, 'w') as fp:
            fp.writestr('somefile.txt', 'this APK is missing AndroidManifest.xml')
        bad_manifest = 'repo/bad-manifest.apk'
        with zipfile.ZipFile(bad_manifest, 'w') as fp:
            fp.writestr('AndroidManifest.xml', 'this is not an AndroidManifest.xml')

        os.environ['ISSUEBOT_CURRENT_APPLICATION_ID'] = 'info.zwanenburg.caffeinetile'
        self.assertTrue(issuebot.IssuebotModule().apkfiles[0].endswith(good))
        assert os.path.exists(good)
        assert os.path.exists(bad + '?')
        assert os.path.exists(bad_manifest + '?')
        assert not os.path.exists(bad)
        assert not os.path.exists(bad_manifest)

    @patch('shutil.which')
    @patch('subprocess.run')
    def test_get_apk_from_google_play_config(self, subprocess_run, shutil_which):
        shutil_which.return_value = '/path/to/apkeep'
        username = 'foobar@gmail.com'
        password = 'secret'  # nosec

        def _subprocess_run(args, env=None):  # pylint: disable=unused-argument
            apkeep_ini = args[4]
            config = configparser.ConfigParser()
            config.read(apkeep_ini)
            self.assertEqual(username, config['google']['username'])
            self.assertEqual(password, config['google']['password'])
            return MagicMock()

        subprocess_run.side_effect = _subprocess_run
        self.assertIsNone(os.environ.get('GOOGLE_PLAY_USERNAME'))
        self.assertIsNone(os.environ.get('GOOGLE_PLAY_PASSWORD'))
        os.environ['GOOGLE_PLAY_USERNAME'] = username
        os.environ['GOOGLE_PLAY_PASSWORD'] = password
        issuebot.get_apk_from_google_play('fake.app.id', set())
        del os.environ['GOOGLE_PLAY_USERNAME']
        del os.environ['GOOGLE_PLAY_PASSWORD']


if __name__ == "__main__":
    os.chdir(os.path.dirname(__file__))
    newSuite = unittest.TestSuite()
    newSuite.addTest(unittest.makeSuite(IssuebotTest))
    unittest.main(failfast=False)
